<?php

namespace Drupal\ai_interpolator_extractor\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for a Email field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_extractor_email",
 *   title = @Translation("Email Extractor"),
 *   field_rule = "email"
 * )
 */
class EmailExtractor extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Email Extractor';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $wrapperEntity) {
      preg_match_all('/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/i', $wrapperEntity->value, $matches);
      $values = [];
      if (isset($matches[0][0])) {
        foreach ($matches[0] as $mail) {
          $values[$mail] = $mail;
        }
      }
    }
    return array_values($values);
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Has to have a link an be valid.
    if (empty($value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
      return FALSE;
    }

    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $entity->set($fieldDefinition->getName(), $values);
  }

}
