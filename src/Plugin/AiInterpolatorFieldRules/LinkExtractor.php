<?php

namespace Drupal\ai_interpolator_extractor\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for a Link field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_extractor_link",
 *   title = @Translation("Link Extractors"),
 *   field_rule = "link"
 * )
 */
class LinkExtractor extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Link Extractor';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_extractor_disallow_extensions'] = [
      '#type' => 'textfield',
      '#title' => 'Disallowed Extensions',
      '#description' => $this->t('A space separated list of all extensions to skip.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_extractor_disallow_extensions', "css js jpg jpeg gif tiff png pdf txt mp3 mp4 mov svg"),
      '#weight' => 24,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $disallowed = explode(" ", $interpolatorConfig['extractor_disallow_extensions']);

    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $wrapperEntity) {
      preg_match_all('/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])/i', $wrapperEntity->value, $matches);
      $values = [];
      if (isset($matches[0][0])) {
        foreach ($matches[0] as $uri) {
          $allow = TRUE;
          foreach ($disallowed as $ext) {
            if (substr($uri, -(strlen($ext) + 1)) == '.' . $ext) {
              $allow = FALSE;
            }
          }
          if ($allow) {
            $values[$uri] = $uri;
          }
        }
      }
    }
    return array_values($values);
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Has to have a link an be valid.
    if (empty($value) || !filter_var($value, FILTER_VALIDATE_URL)) {
      return FALSE;
    }

    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    foreach ($values as $key => $value) {
      $new['uri'] = $value;
      if ($config['title'] == 0) {
        $new['title'] = '';
      }
      $values[$key] = $new;
    }
    $entity->set($fieldDefinition->getName(), $values);
  }

}
