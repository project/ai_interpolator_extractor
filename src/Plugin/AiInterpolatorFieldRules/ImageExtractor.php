<?php

namespace Drupal\ai_interpolator_extractor\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for an image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_extractor_image",
 *   title = @Translation("Image Extractor"),
 *   field_rule = "image",
 *   target = "file"
 * )
 */
class ImageExtractor extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Image Extractor';

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {

    $form['interpolator_extractor_offset'] = [
      '#type' => 'number',
      '#title' => 'Offset',
      '#description' => $this->t('Offset to start from. If you want to start from the second one, you write 2 here.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_extractor_offset', ''),
      '#weight' => 24,
    ];

    $form['interpolator_extractor_amount'] = [
      '#type' => 'number',
      '#title' => 'Amount',
      '#description' => $this->t('Amount to get. If nothing is set it gets all if that is possible.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_extractor_amount', ''),
      '#weight' => 24,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $wrapperEntity) {
      preg_match_all("/(http[s]*:\/\/)([a-z\-_0-9\/.]+)\.([a-z.]{2,3})\/([a-z0-9\-_\/._~:?#\[\]@!$&'()*+,;=%]*)([a-z0-9]+\.)(gif|jpg|jpeg|png)/i", $wrapperEntity->value, $matches);
      $values = [];
      if (isset($matches[0][0])) {
        foreach ($matches[0] as $mail) {
          $values[$mail] = $mail;
        }
      }
    }
    return array_values($values);
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    if (!filter_var($value, FILTER_VALIDATE_URL)) {
      return FALSE;
    }
    // File is allowed.
    $allowedExtension = FALSE;
    foreach (explode(" ", $config['file_extensions']) as $ext) {
      if (substr($value, -(strlen($ext))) == $ext) {
        $allowedExtension = TRUE;
      }
    }

    if (!$allowedExtension) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    $amount = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_extractor_amount', '');
    $offset = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_extractor_offset', '');
    $count = 0;
    foreach ($values as $value) {
      $count++;
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/' . basename($value);
      // Get resolution.
      $resolution = getimagesize($value);
      // Check if resolution fits.
      if (!$this->fitsResolution($resolution, $config)) {
        continue;
      }
      if ($offset && $count < $offset) {
        continue;
      }
      // Create file entity from string.
      $file = $this->generateFileFromString(file_get_contents($value), $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality() || $successFul == $amount) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, -(strlen($fileName) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }

  /**
   * Check if resolution fits.
   *
   * @param array $resolution
   *   The resolution.
   * @param array $config
   *   The config.
   *
   * @return bool
   *   If it fits or not.
   */
  protected function fitsResolution($resolution, $config) {
    $max = explode('x', $config['max_resolution']);
    $min = explode('x', $config['min_resolution']);

    if (count($max) > 1 && ($resolution[0] >= $max[0] || $resolution[1] >= $max[1])) {
      return FALSE;
    }
    if (count($min) && ($resolution[0] <= $min[0] || $resolution[1] <= $min[1])) {
      return FALSE;
    }
    return TRUE;
  }

}
