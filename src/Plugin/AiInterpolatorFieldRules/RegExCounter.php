<?php

namespace Drupal\ai_interpolator_extractor\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for a RegEx field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_counter_regex_counter",
 *   title = @Translation("RegEx Counter"),
 *   field_rule = "integer"
 * )
 */
class RegExCounter extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'RegEx Counter';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'text_long',
      'text',
      'string',
      'string_long',
      'text_with_summary',
      'link',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_extractor_regex'] = [
      '#type' => 'textfield',
      '#title' => 'Regular Expression',
      '#description' => $this->t('The regular expression to extract from. Try you rule on <a href="https://regex101.com/" target="blank">https://regex101.com/</a>.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_extractor_regex', ""),
      '#weight' => 24,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $fieldType = $entity->{$interpolatorConfig['base_field']}->getFieldDefinition()->getType();
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $wrapperEntity) {
      $matches = [];
      try {
        switch ($fieldType) {
          case 'link':
            $values[] = preg_match_all($interpolatorConfig['extractor_regex'], htmlspecialchars_decode($wrapperEntity->uri), $matches) ?? 0;
            break;
          default:
            $values[] = preg_match_all($interpolatorConfig['extractor_regex'], htmlspecialchars_decode($wrapperEntity->value), $matches) ?? 0;
        }
      }
      catch (\Exception $e) {
        $values[] = 0;
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Has to have a link an be valid.
    if (empty($value) || !is_numeric($value)) {
      return FALSE;
    }

    // Otherwise it is ok.
    return TRUE;
  }

}
